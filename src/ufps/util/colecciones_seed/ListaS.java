/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase que representa la colección Lista Simple enlazada
 * @author madarme
 */
public class ListaS<T> {
    
    //No hace falta la inicialización (opcional)
    private Nodo<T> cabeza=null;
    private int tamanio=0; //<--Sólo se altera con las operaciones de inserción y/o borrado

    public ListaS() {
    }

    public int getTamanio() {
        return tamanio;
    }
    /**
     * Método adiciona un elemento al inicio de la lista
     * @param datoNuevo objeto a insertar en la lista simple
     */
    
    
    
    public void insertarInicio(T datoNuevo)
    {
    /**
     * Estrategia computacional:
            1. Objeto nuevo en Nodo_nuevo
            2. El sig de Nuevo_nodo es la cabeza
            3. cabeza=Nuevo_nodo
            4. aumentar tamaño
     */
        
        Nodo<T> nuevoNodo=new Nodo(datoNuevo,this.cabeza);
        this.cabeza=nuevoNodo;
        this.tamanio++;
    }

    public void insertarFin(T datoNuevo)
    {
        if(this.esVacia())
            this.insertarInicio(datoNuevo);
        else
        {
        
            
            try {
                Nodo<T> nodoNuevo=new Nodo(datoNuevo, null);
                Nodo<T> nodoUltimo=getPos(this.tamanio-1);
                //Unimos:
                nodoUltimo.setSig(nodoNuevo);
                this.tamanio++;
                
                
            } catch (Exception ex) {
                System.err.println(ex.getMessage());
            }
            
        }
    }
    
    
    public T get(int i)
    {
        
        try {
            return this.getPos(i).getInfo();
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return null;
        }
    }
    
    
    public void set(int i,T datoNuevo)
    {
        
        try {
            this.getPos(i).setInfo(datoNuevo);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            
        }
    }
    
    
    /**
     * Método borrar el nodo de la posición i
     * @param i un entero que representa el índice de un nodo
     * @return el objeto almacenado en esa posición
     */
    public T eliminar(int i)
    {
        if(this.esVacia())
            return null;
        //Caso 1: i en cabeza , i==0
        Nodo<T> nodoBorrar=null;
        if(i==0)
        {
            nodoBorrar=this.cabeza;
            this.cabeza=this.cabeza.getSig();
            
        }
        else
        {
            try {
                Nodo<T> nodoAnterior=this.getPos(i-1);
                nodoBorrar=nodoAnterior.getSig();
                //Unir :
                nodoAnterior.setSig(nodoBorrar.getSig());
                
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                return null;
            }
        }
        
        nodoBorrar.setSig(null);
        this.tamanio--;
        return nodoBorrar.getInfo();
    }
    
    
    private Nodo<T> getPos(int i) throws Exception //Excepción obligatoria su tratamiento
    {
        if(i<0 || i>=this.tamanio)
            throw new Exception("índice fuera de rango:"+i);
        
        Nodo<T> nodoPos=this.cabeza;
        while(i>0)
        {
            nodoPos=nodoPos.getSig();
            i--;
        }
        return nodoPos;
    }
    
    @Override
    public String toString() {
        
        if(this.esVacia())
            return "La lista no contiene elementos";
        
        String msg="";
        //vector--> pos en 0 --> hasta length-1
        //Desde la cabeza hasta null
        Nodo<T> nodoActual=this.cabeza;
        while(nodoActual!=null)
        {
            T info=nodoActual.getInfo();
            //Para esto es necesario que la clase en T tenga toString
            msg+=info.toString()+"->";
            nodoActual=nodoActual.getSig();
        }
     return "Cabeza->"+msg+"null";
    }
    
    public boolean esVacia()
    {
        return this.cabeza==null;
    }
    
    /**
     * Elimina todo dato repetido de la lista simple 
     * y deja una sóla incidencia
     * Por ejemplo: L=<3,5,6,3,3,5,7,8> 
     *  L.eliminarRepetidos()--> L=<3,5,6,7,8>
     * 
     * --> 2 primeros ejercicios reciben un bono de 0.5 sobre el P1
     */
    public void eliminarRepetidos()
    {
        //:)
    }
    
}
